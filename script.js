function ChessBoard(figures) {

// initialize board
    this.createBoard = function () {
        this.squares =  [];
        this.figures =  [];
        this.lightColor =  '#f0d9b5';
        this.darkColor =  '#b58863';
        this.el = document.createElement('div');
        this.el.setAttribute('id', 'board');
        this.el.setAttribute('style', 'width: 600px; height: 600px; border: 4px solid black');
        document.getElementsByClassName('container')[0].append(this.el);
    };

// append squares to board, giving appearance as chess board
    this.appendSquares = function () {
        var square;
        var letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'];

        // set one square width from dividing board to 8, get equal 8 parts
        var width = parseInt(this.el.style.width) / 8 + 'px';

        // create squares, get their div elements and append them row by row to board
        for (var j = 1; j <= 8; j++) {
            var row = document.createElement('div');
            row.setAttribute('class', 'row');

            for (var k = 0; k < letters.length; k++) {
                if ((k + j) % 2 !== 0) {
                    //new Square(position, color, width)
                    square = new Square(letters[k] + j, this.lightColor, width);
                } else {
                    square = new Square(letters[k] + j, this.darkColor, width);
                }

                // add square-div to row
                row.appendChild(square.el);

                // add the square to the board squares' array
                this.squares.push(square);
            }
            
            // append the row to chess board div
            document.getElementById('board').appendChild(row);
        }
    };

    // return square object if is given its position
    this.findSquareByPos = function (pos) {
        for (var i = 0; i < this.squares.length; i++) {
            if (this.squares[i].pos === pos) {
                return this.squares[i];
            }
        }
    };

// append figures to board
    this.appendFigures = function (figures) {
        for (var i = 0; i < figures.length; i++) {
            var figure = new Figure(figures[i]);
            var square = this.findSquareByPos(figure.position);
            square.setFigure(figure);
            this.figures.push(figure);
        }
    };

    this.createBoard();
    this.appendSquares();
    this.appendFigures(figures);

// listen to click events
    this.el.addEventListener('click', clickOnBoard.bind(this));

// listen to mousedown events
    this.el.addEventListener('mousedown', mousedownOnBoard.bind(this));

// set div with figure focused and show figure potential movies
    this.setFocused = function (figurePosition, poses) {
        // valid movies
        var validMoveDivs = [];
        // get position directions, like vertical, diagonal, ...
        for (var posDir in poses) {
            //if direction is empty skip it.
            if (!(poses[posDir].length > 0)) continue;

            // this statement is true for king and rook
            if (posDir === 'allDirs') {
                for (var i = 0; i <= poses[posDir].length; i++) {

                    var sq = this.findSquareByPos(poses[posDir][i]);
                    if (sq) {
                        sq.setClicked(true);
                        validMoveDivs.push(sq.pos);
                    }
                }
            }

            var figurePosIndex = poses[posDir].indexOf(figurePosition);

            for (var i = figurePosIndex; i >= 0; i--) {

                var sq = this.findSquareByPos(poses[posDir][i]);
                if (sq) {
                    if (sq.hasFigure && poses[posDir][i] !== figurePosition) {
                        sq.setClicked(true);
                        validMoveDivs.push(sq.pos);
                        break;
                    }
                    sq.setClicked(true);
                    validMoveDivs.push(sq.pos);
                }
            }

            for (i = figurePosIndex; i < poses[posDir].length; i++) {

                sq = this.findSquareByPos(poses[posDir][i]);
                if (sq) {
                    if (sq.hasFigure && poses[posDir][i] !== figurePosition) {
                        sq.setClicked(true);
                        validMoveDivs.push(sq.pos);
                        break;
                    }
                    sq.setClicked(true);
                    validMoveDivs.push(sq.pos);
                }
            }
        }
        return  validMoveDivs;
    };

// get figure by its position
    this.getFigure = function (pos) {
        for (var i = 0; i < this.figures.length; i++) {
            if (this.figures[i].position === pos) {
                return this.figures[i];
            }
        }
    };

// clear focus from others
    this.blurOthers = function () {
        for (var sq = 0; sq < this.squares.length; sq++) {
            this.squares[sq].setClicked(false);
        }
    };

// return square object if given its element
    this.findSquare = function (el) {
        for (var i = 0; i < this.squares.length; i++) {
            if (this.squares[i].el === el) {
                return this.squares[i];
            }
        }
    };


// click callback
    function clickOnBoard (e) {
        this.blurOthers();
        // on fast mousemove we may loose click element, so for sure, we must check if there is clicked element
        if (this.findSquare(e.target)) {
            var clickedSq = this.findSquare(e.target);
            clickedSq.setClicked(true);
            if (clickedSq.hasFigure) {
                var figure = this.getFigure(clickedSq.pos);
                figure.clicked = true;
                var potentialMoves = figure.getPotentialMovePoses(figure);
                this.setFocused(figure.position, potentialMoves);
            }
        }
    }

// mousedown callback
    function mousedownOnBoard (e) {
        e.preventDefault();
        var clickedSquare = this.findSquare(e.target);
        if (!clickedSquare.hasFigure) return;
        console.log('444');
        console.log(clickedSquare);
        var clickedFigure = this.getFigure(clickedSquare.pos);
        clickedFigure.setDraggable(true);


        this.blurOthers();
        var offsetX = e.clientX - clickedFigure.el.offsetLeft;
        var offsetY = e.clientY - clickedFigure.el.offsetTop;

        // add event listeners
        document.addEventListener('mousemove', mouseMoveHandler.bind(this));
        document.addEventListener('mouseup', mouseUpHandler.bind(this));

        function mouseMoveHandler (e) {
            e.preventDefault();
            if (clickedFigure.draggable) {
                // clickedSquare.hasFigure = false;
                clickedFigure.setStyle({
                    zIndex: 2,
                    left: e.clientX  - offsetX+ 'px',
                    top: e.clientY  - offsetY + 'px'
                });

                this.blurOthers();
                var hoveredDiv = this.findSquare(e.target);
                hoveredDiv.setClicked(true);

            }
        }

        function mouseUpHandler (e) {
            if (clickedFigure.draggable) {
                var potentialMoves = clickedFigure.getPotentialMovePoses(clickedFigure);
                var hoveredDiv = this.findSquare(e.target);
                if (this.setFocused(clickedFigure.position, potentialMoves).indexOf(hoveredDiv.pos) > -1) {
                    clickedSquare.hasFigure = false;
                    clickedFigure.position = hoveredDiv.pos;
                    hoveredDiv.setFigure(clickedFigure);
                } else {
                    // if move is not valid set back the figure
                    console.log('33');
                    console.log(clickedSquare);
                    clickedFigure.position = clickedSquare.pos;
                    clickedSquare.setFigure(clickedFigure);
                }
                clickedFigure.setStyle({
                    left: '',
                    top: '',
                    zIndex: 1
                });
                clickedFigure.setDraggable(false);

                // on mouseup event removing evenetListeners for mousemove and mouseup
                document.removeEventListener("mousemove", mouseMoveHandler.bind(this));
                document.removeEventListener("mouseup", mouseUpHandler.bind(this));
            }
        }
    }



}

ChessBoard.prototype = {
    setStyle: function (obj) {
        for (var k in obj) {
            this.el.style[k] = obj[k];
        }
        return this;
    },
    setColors: function (light, dark) {
        this.lightColor = light;
        this.darkColor = dark;
        return this;
    }
};


// SQUARE
function Square (pos, color, width) {
    this.clicked = false;
    this.hasFigure = false;
    this.pos = pos;
    this.color = color;
    this.len = width;


    this.createSquare = function () {
        this.el = document.createElement('div');
        this.setStyle({
            backgroundColor: this.color,
            width: this.len,
            height: this.len
        });
        this.el.setAttribute('class', 'square square-' + this.pos);
    };

    this.createSquare();
}

Square.prototype = {
    setClicked: function (bool) {
        if (bool) {
            if (this.hasFigure) {
                this.setStyle({border: '2px solid red'});
            } else {
                this.setStyle({border: '2px solid green'});
            }

            this.el.clicked = true;
        } else {
            this.setStyle({border: 'none'});
            this.el.clicked = false;
        }
    },
    setFigure: function (figureObj) {
        this.hasFigure = true;
        this.el.appendChild(figureObj.el);
    },
    setStyle: function (obj) {
        for (var k in obj) {
            this.el.style[k] = obj[k];
        }
        return this;
    }
};


// FIGURE
function Figure (paramObj) {
    this.type = paramObj.type;
    this.position = paramObj.position;
    this.color = paramObj.color;
    this.clicked = false;
    this.draggable = false;
    this.createFigure = function () {
        this.el = document.createElement('img');
        this.el.setAttribute('src', './images/' + this.type + '_' + this.color + '.svg');
    };

    this.createFigure();
}

Figure.prototype = {
    getPotentialMovePoses: function (figure) {
        var letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'];
        var numbers = [1, 2, 3, 4, 5, 6, 7, 8];

        var letterPos = letters.indexOf(figure.position.charAt(0));
        var numberPos = numbers.indexOf(parseInt(figure.position.charAt(1)));
        var poses =  {
            diagonal: [],
            diagonalOpp: [],
            vertical: [],
            horizontal: [],
            allDirs: []
        };

        var figureMap = {
            King: function () {
                for (var i = letterPos - 1; i <= letterPos + 1; i++) {
                    for (var j = numberPos - 1; j <= numberPos + 1; j++) {
                        poses.allDirs.push(letters[i] + numbers[j]);
                    }
                }

                return poses;
            },

            Bishop: function () {
                var diff = letterPos - numberPos;
                for (var i = 0; i < letters.length; i++) {
                    for (var j = 0; j < numbers.length; j++) {
                        if (i === j + diff) {
                            poses.diagonalOpp.push(letters[i] + numbers[j]);
                        }

                        if (j === -i + letterPos + numberPos) {
                            poses.diagonal.push(letters[i] + numbers[j]);
                        }
                    }
                }
                return poses;
            },

            Rook: function () {
                for (var i = 0; i < letters.length; i++) {
                    for (var j = 0; j < numbers.length; j++) {
                        if (i === letterPos) {
                            poses.vertical.push(letters[i] + numbers[j]);
                        }

                        if (j === numberPos) {
                            poses.horizontal.push(letters[i] + numbers[j]);
                        }
                    }
                }
                return poses;
            },

            Pawn: function () {
                if (figure.color === 'white') {
                    // in row 2
                    if (numberPos === 1) {
                        poses.vertical.push(letters[letterPos] + numbers[numberPos + 2]);
                        poses.vertical.push(letters[letterPos] + numbers[numberPos + 1]);
                    } else {
                        poses.vertical.push(letters[letterPos] + numbers[numberPos + 1]);
                    }

                } else {
                    // in row 7
                    if (numberPos === 6) {
                        poses.vertical.push(letters[letterPos] + numbers[numberPos - 2]);
                        poses.vertical.push(letters[letterPos] + numbers[numberPos - 1]);
                    } else {
                        poses.vertical.push(letters[letterPos] + numbers[numberPos - 1]);
                    }
                }

                // self position
                poses.vertical.push(letters[letterPos] + numbers[numberPos]);

                return poses;
            },

            Knight: function () {
                poses.allDirs =  poses.allDirs.concat(
                    letters[letterPos - 2] + numbers[numberPos + 1],
                    letters[letterPos + 2] + numbers[numberPos + 1],
                    letters[letterPos - 2] + numbers[numberPos - 1],
                    letters[letterPos + 2] + numbers[numberPos - 1],
                    letters[letterPos + 1] + numbers[numberPos + 2],
                    letters[letterPos + 1] + numbers[numberPos - 2],
                    letters[letterPos - 1] + numbers[numberPos + 2],
                    letters[letterPos - 1] + numbers[numberPos - 2]
                );

                return poses;
            },

            Qween: function () {
                //like bishop
                var diff = letterPos - numberPos;
                for (var i = 0; i < letters.length; i++) {
                    for (var j = 0; j < numbers.length; j++) {
                        if (i === j + diff) {
                            poses.diagonalOpp.push(letters[i] + numbers[j]);
                        }

                        if (j === -i + letterPos + numberPos) {
                            poses.diagonal.push(letters[i] + numbers[j]);
                        }
                    }
                }

                // like rook
                for (i = 0; i < letters.length; i++) {
                    for (j = 0; j < numbers.length; j++) {
                        if (i === letterPos) {
                            poses.vertical.push(letters[i] + numbers[j]);
                        }

                        if (j === numberPos) {
                            poses.horizontal.push(letters[i] + numbers[j]);
                        }
                    }
                }


                return poses;
            }
        };

        getfigureMove = figureMap[figure.type];

        return getfigureMove();
    },
    setDraggable: function(bool) {
        this.el.setAttribute('draggable', bool);
        this.draggable = bool;
        return this;
    },
    setStyle: function (obj) {
        for (var k in obj) {
            this.el.style[k] = obj[k];
        }
        return this;
    }
};


// CREATE BOARD
var chessBoard = new ChessBoard([
    {
        type: "King",
        position: "c2",
        color: "black"
    },
    {
        type: "Rook",
        position: "e6",
        color: "black"
    },
    {
        type: "Bishop",
        position: "c7",
        color: "white"
    },
    {
        type: "Pawn",
        position: "g7",
        color: "black"
    },
    {
        type: "Pawn",
        position: "d2",
        color: "white"
    },
    {
        type: "Pawn",
        position: "c4",
        color: "white"
    },
    {
        type: "Pawn",
        position: "d7",
        color: "black"
    },
    {
        type: "Pawn",
        position: "h5",
        color: "black"
    },
    {
        type: "Knight",
        position: "d6",
        color: "black"
    },
    {
        type: "Qween",
        position: "g3",
        color: "white"
    },
    {
        type: "Qween",
        position: "d4",
        color: "black"
    }
]);
